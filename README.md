Integrating JACOCO and SONAR with Spring Boot Application
=========================================================

Shows how to integrate jacoco and sonar with a spring boot application
----------------------------------------------------------------------

* jacoco for code coverage
* sonar for code quality
* send code coverage details to sonar


Requirements
------------

* [Java Platform (JDK) 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Apache Maven 3.x](http://maven.apache.org/)
* [Ensure Sonar is running


Running the application
-----------------------

* mvn sonar:sonar
* mvn sonar:sonar -Dsonar.host.url=http://192.168.1.6:9000
* view details in the sonar user interface


References
----------

* http://dev.macero.es/2016/02/06/test-coverage-analysis-for-your-spring-boot-app/
* JHipster

