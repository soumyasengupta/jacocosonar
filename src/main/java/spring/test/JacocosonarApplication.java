package spring.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacocosonarApplication {

	public static void main(String[] args) {
		SpringApplication.run(JacocosonarApplication.class, args);
	}
}
